# Calendar.social
# Copyright (C) 2018  Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Helper functions for testing
"""

from contextlib import contextmanager

import calsocial
from calsocial.models import db


def configure_app():
    """Set default configuration values for testing
    """

    calsocial.app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'
    calsocial.app.config['TESTING'] = True
    calsocial.app.config['WTF_CSRF_ENABLED'] = False


def login(client, username, password, no_redirect=False):
    """Login with the specified username and password
    """

    return client.post('/login',
                       data={'email': username, 'password': password},
                       follow_redirects=not no_redirect)


@contextmanager
def alter_config(app, **kwargs):
    saved = {}

    for key, value in kwargs.items():
        if key in app.config:
            saved[key] = app.config[key]

        app.config[key] = value

    yield

    for key, value in kwargs.items():
        if key in saved:
            app.config[key] = saved[key]
        else:
            del app.config[key]
