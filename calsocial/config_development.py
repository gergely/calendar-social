"""Configuration file for the development environment
"""

ENV = 'development'
#: If ``True``, registration on the site is enabled.
REGISTRATION_ENABLED = True
#: The default time zone
DEFAULT_TIMEZONE = 'Europe/Budapest'

DEBUG = True
SQLALCHEMY_DATABASE_URI = 'sqlite:///local.db'
SQLALCHEMY_TRACK_MODIFICATIONS = False
SECRET_KEY = 'ThisIsNotSoSecret'
SECURITY_PASSWORD_HASH = 'bcrypt'
SECURITY_PASSWORD_SALT = SECRET_KEY
SECURITY_REGISTERABLE = False
CACHE_TYPE = 'simple'
