# Calendar.social
# Copyright (C) 2018 Gergely Polonkai
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""Calendar system base definition for Calendar.social
"""

class CalendarSystem:
    """Base class for calendar systems
    """

    __has_months__ = False
    __has_weeks__ = False

    @property
    def day_names(self):
        """An iterable of day names
        """

        raise NotImplementedError()

    @property
    def month(self):
        """The name of the current month, if applicable

        It may include the year.
        """

        raise NotImplementedError()

    @property
    def days(self):
        """An iterable of days to be displayed on the month view, if applicable
        """

        raise NotImplementedError()
