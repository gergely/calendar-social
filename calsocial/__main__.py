"""Main module for Calendar.social so the app can be run directly.
"""

from calsocial import CalendarSocialApp


app = CalendarSocialApp('calsocial')

app.run()
